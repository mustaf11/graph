package eg.edu.alexu.csd.filestructure.graphs.cs50_cs42;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.LinkedList;
import java.util.PriorityQueue;
import java.util.Queue;
import java.util.Scanner;

import eg.edu.alexu.csd.filestructure.graphs.IGraph;
import javafx.util.Pair;

public class GraphImp implements IGraph {

	private class Entry implements Comparable<Entry> {
		private Integer key;
		private Integer value;

		public Entry(Integer key, Integer value) {
			this.key = key;
			this.value = value;
		}

		public Integer getKey() {
			return key;
		}

		public Integer getValue() {
			return value;
		}

		@Override
		public int hashCode() {
			return key.hashCode() + value.hashCode();
		}

		@Override
		public boolean equals(Object o) {
			if (!(o instanceof Entry)) {
				return false;
			}
			Entry e = (Entry) o;
			return this.key.equals(e.getKey()) && this.value.equals(e.getValue());
		}

		@Override
		public int compareTo(Entry e) {
			return this.getKey().compareTo(e.getKey());
		}

	}

	private ArrayList<ArrayList<Pair<Integer, Integer>>> adjList;
	private ArrayList<Integer> vertices;
	private int m;
	private ArrayList<Integer> dijkstra;

	@Override
	public void readGraph(File file) {
		try {
			Scanner in = new Scanner(file);
			int n = in.nextInt();
			m = in.nextInt();
			adjList = new ArrayList<>();
			vertices = new ArrayList<>();
			for (int i = 0; i < n; i++) {
				adjList.add(new ArrayList<>());
				vertices.add(i);
			}
			for (int i = 0; i < m; i++) {
				int from = in.nextInt();
				int to = in.nextInt();
				int weight = in.nextInt();
				adjList.get(from).add(new Pair<Integer, Integer>(to, weight));
			}
			in.close();
		} catch (FileNotFoundException e) {
			throw new RuntimeException();
		}

	}

	@Override
	public int size() {
		return m;
	}

	@Override
	public ArrayList<Integer> getVertices() {
		return vertices;
	}

	@Override
	public ArrayList<Integer> getNeighbors(int v) {
		ArrayList<Integer> neighbors = new ArrayList<>();
		for (int i = 0; i < adjList.get(v).size(); i++) {
			neighbors.add(adjList.get(v).get(i).getKey());
		}
		return neighbors;
	}

	@Override
	public void runDijkstra(int src, int[] distances) {
		dijkstra = new ArrayList<>();
		PriorityQueue<Entry> pQueue = new PriorityQueue<>();
		for (int i = 0; i < distances.length; i++) {
			pQueue.add(new Entry(Integer.MAX_VALUE / 2, i));
			distances[i] = Integer.MAX_VALUE / 2;
		}
		pQueue.remove(new Entry(Integer.MAX_VALUE / 2, src));
		pQueue.add(new Entry(0, src));
		distances[src] = 0;
		while (!pQueue.isEmpty()) {
			Integer s = pQueue.poll().getValue();
			dijkstra.add(s);
			for (int i = 0; i < adjList.get(s).size(); i++) {
				int d = adjList.get(s).get(i).getKey();
				int weight = adjList.get(s).get(i).getValue();
				if(distances[d] > (distances[s] + weight)) {
					pQueue.remove(new Entry(distances[d], d));
					pQueue.add(new Entry(distances[s] + weight, d));
					distances[d] = distances[s] + weight;
				}
			}
		}
	}

	@Override
	public ArrayList<Integer> getDijkstraProcessedOrder() {
		return dijkstra;
	}

	@Override
	public boolean runBellmanFord(int src, int[] distances) {
		boolean[] inQueue = new boolean[vertices.size()];
		int[] numOfVisits = new int[vertices.size()];
		Arrays.fill(distances, Integer.MAX_VALUE / 2);
		Queue<Integer> q = new LinkedList<>();
		q.add(src);
		inQueue[src] = true;
		numOfVisits[src]++;
		distances[src] = 0;
		while (!q.isEmpty()) {
			int x = q.poll();
			inQueue[x] = false;
			if (numOfVisits[x] > vertices.size())
				return false;
			for (int i = 0; i < adjList.get(x).size(); i++) {
				int to = adjList.get(x).get(i).getKey();
				int weight = adjList.get(x).get(i).getValue();
				if (distances[to] > distances[x] + weight) {
					distances[to] = distances[x] + weight;
					if (!inQueue[to]) {
						q.add(to);
						inQueue[to] = true;
						numOfVisits[to]++;
					}
				}
			}
		}
		return true;
	}

}
